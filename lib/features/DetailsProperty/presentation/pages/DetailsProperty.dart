import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:web_application/features/Home/presentation/widgets/CustomCard.dart';

class DetailsProperty extends StatefulWidget {
  static const routeName = '/DetailsProperty';

  @override
  _DetailsPropertyState createState() => _DetailsPropertyState();
}

class _DetailsPropertyState extends State<DetailsProperty> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightBlue,
        title: Center(
          child: Text(
            ' التفاصيل',
            textAlign: TextAlign.center,
          ),
        ),
      ),
      body: Column(
        children: [
          Image(
            image: AssetImage("assets/images/insideHome (2).jpeg"),
            fit: BoxFit.cover,
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 8.0,horizontal: 8.0),
            child: MaterialButton(
              onPressed: () {
                setState(() {});
              },
              child: Text(
                "بيع",
                style: TextStyle(
                    fontSize: 13.0.sp,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
              color: Colors.lightBlue,
//    elevation: 5,
              minWidth: 95.0.w,
              height: 8.0.h,

              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
            ),
          ),
          Text("data"),
          Text("data"),
          Text("data"),
          Text("data"),
          Text("data"),
          Text("data"),
          Text("data"),
          Text("data"),
        ],
      ),
    );
  }
}
