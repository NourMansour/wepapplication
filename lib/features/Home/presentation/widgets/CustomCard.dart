import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:sizer/sizer.dart';

class CustomCard extends StatelessWidget {
  final Function onPressEdit;
  final Function onPressDelete;
  final Function onPressCard;
  final String title;
  final String description;
  CustomCard(
      {this.onPressEdit,
      this.onPressDelete,
      this.title,
      this.description,
      this.onPressCard});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 1.0.h, vertical: 0.5.h),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5), //color of shadow
                spreadRadius: 5, //spread radius
                blurRadius: 7, // blur radius
                offset: Offset(0, 2), // changes position of shadow
              ),
              //you can set more BoxShadow() here
            ],
          ),
          child: Column(
            children: [
              ListTile(
                title: Text(
                  title,
                ),
                subtitle: Text(
                  description,
                ),
                leading: InkWell(
                  child: Icon(
                    Icons.edit,
                    color: Colors.green,
                  ),
                  onTap: onPressEdit,
                ),
                trailing: InkWell(
                  child: Icon(
                    Icons.delete_forever_outlined,
                    color: Colors.redAccent,
                  ),
                  onTap: onPressDelete,
                ),
              ),
            ],
          ),
        ),
      ),
      onTap: onPressCard,
    );
  }
}
