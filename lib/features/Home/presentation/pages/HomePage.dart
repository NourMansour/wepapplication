import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:web_application/features/AddProperty/presentation/pages/AddProperty.dart';
import 'package:web_application/features/DetailsProperty/presentation/pages/DetailsProperty.dart';
import 'package:web_application/features/EditProperty/presentation/pages/EditProperty.dart';

import 'package:web_application/features/Home/presentation/widgets/CustomCard.dart';

class HomePage extends StatefulWidget {
  static const routeName = '/HomePage';

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 0,
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightBlue,
          title: Center(
              child: Text(
            'الصفحة الرئيسية',
            textAlign: TextAlign.center,

          )),
          bottom: TabBar(
            tabs: <Widget>[
              Tab(
                icon: Icon(Icons.home),
              ),
              Tab(
                icon: Icon(Icons.settings),
              ),
              Tab(
                icon: Icon(Icons.history),
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            contentFirstTab(),
            contentSecondTab(),
            contentThirdTab(),
          ],
        ),
      ),
    );
  }

  contentFirstTab() {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
        onPressed: () {
          setState(() {
            Navigator.push(context,      CupertinoPageRoute(
                builder: (context) => AddProperty()
            ),);
          });
        },
      ),
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/home.jpg"),
                fit: BoxFit.cover,
//                  colorFilter: ColorFilter.mode(AppTheme.appGreyColor, BlendMode.color),
              ),
            ),
            padding: EdgeInsets.symmetric(horizontal: 20.0.h),
          ),
          Center(
              child: Container(
            child: ListView.builder(
              itemCount: 15,
              itemBuilder: (context, index) {
                return CustomCard(
                  onPressEdit: () {
                    setState(() {
                      Navigator.push(context,      CupertinoPageRoute(
                          builder: (context) => EditProperty()
                      ),);
                    });

                  },
                  onPressDelete: () {},
                  title: "العقار ",
                  description: "شقة سكنية",
                  onPressCard: ()
                  {
                    setState(() {
                      Navigator.push(context,      CupertinoPageRoute(
                          builder: (context) => DetailsProperty()
                      ),);
                    });

                  },
                );
              },
            ),
          )),
        ],
      ),
    );
  }

  contentSecondTab() {
    TextEditingController valueController = TextEditingController();
    return Scaffold(
      body: Center(
          child: Container(
        child: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/home.jpg"),
                  fit: BoxFit.cover,
//                  colorFilter: ColorFilter.mode(AppTheme.appGreyColor, BlendMode.color),
                ),
              ),
              padding: EdgeInsets.symmetric(horizontal: 20.0.h),
            ),

            Padding(
              padding:  EdgeInsets.symmetric(horizontal:1.0.h,vertical: 8.0.h),
              child: Container(
                color: Colors.blueGrey,
                child: Padding(
                  padding:  EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: TextFormField(

                          controller: valueController,
                          decoration: InputDecoration(
                              border: UnderlineInputBorder(),
                              hintText: 'Enter value'),

                        ),
                      ),
                      Text("Key"),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      )),
    );
  }

  contentThirdTab() {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/home.jpg"),
                fit: BoxFit.cover,
//                  colorFilter: ColorFilter.mode(AppTheme.appGreyColor, BlendMode.color),
              ),
            ),
            padding: EdgeInsets.symmetric(horizontal: 20.0.h),
          ),

          Center(
              child: Container(
            child: ListView.builder(
              itemCount: 5,
              itemBuilder: (context, index) {
                return Card(
                  child: Text("مباع"),
                );
              },
            ),
          )),
        ],
      ),
    );
  }
}
