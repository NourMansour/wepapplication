import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:web_application/core/CoreModels/base_response_model.dart';
import 'package:web_application/core/errors/base_error.dart';
import 'package:web_application/core/utils/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:web_application/features/User/data/LoginRequestModel.dart';
import 'package:web_application/features/User/data/LoginResponseModel.dart';
import 'package:web_application/features/User/domain/repo/UserRepository.dart';

part 'user_state.dart';

class UserCubit extends Cubit<UserState> {
  UserCubit() : super(UserInitial());

  Future<void> login(String username , String password)async
  {

    emit(LoginLoading());

    var response= await UserRepository.login(
        LoginRequestModel(
            userNameOrEmailAddress: username,
            password: password,
            rememberClient: true
        )
    );

        if(response is LoginResponseModel){
          //todo
          LoginResponseModel responseModel = response;
          emit(LoginSuccessfully());

          AppSharedPreferences.accessToken = responseModel.accessToken;

        }

        else if(response is BaseError){
          print(response.message);
          emit(LoginError(response.message));
        }

        else if (response is ServerError){
          emit(LoginError(response.details));
        }
  }
}
