import 'package:web_application/core/CoreModels/base_result_model.dart';
import 'package:web_application/core/data_source/remote_data_source.dart';
import 'package:web_application/core/http/api_urls.dart';
import 'package:web_application/core/http/http_method.dart';
import 'package:web_application/features/User/data/LoginRequestModel.dart';
import 'package:web_application/features/User/data/LoginResponseModel.dart';

class UserRepository {
  static Future<BaseResultModel> login(LoginRequestModel loginModel) async {
    return await RemoteDataSource.request<LoginResponseModel>(
        converter: (json) => LoginResponseModel.fromJson(json),
        method: HttpMethod.POST,
        data: loginModel.toJson(),
        url: ApiURLs.O_AUTH_LOGIN);
  }
}
