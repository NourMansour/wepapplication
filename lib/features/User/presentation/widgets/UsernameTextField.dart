import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:web_application/core/widgets/BaseTextField.dart';
class UsernameTextField extends StatelessWidget {

  final TextEditingController textEditingController;
  final TextInputAction action;

  const UsernameTextField({Key key, this.textEditingController, this.action}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BaseTextField(
      textEditingController: textEditingController,
      action: action,
      labelText: "username".tr(),
    );
  }
}