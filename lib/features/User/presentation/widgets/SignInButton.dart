import 'package:flutter/material.dart';
import 'package:web_application/core/constants/AppColors.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:sizer/sizer.dart';
class SignInButton extends StatelessWidget {
  final VoidCallback onPressed;
final String title;
  const SignInButton({Key key, this.onPressed, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: onPressed,
      child: Text(
        title,
        style: TextStyle(
            fontSize: 13.0.sp,
            fontWeight: FontWeight.bold,
            color: Colors.white),
      ),
      color:  Colors.lightBlue,
      elevation: 5,
      minWidth: 90.0.w,
      height: 8.0.h,

      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10)),
    );
  }
}
