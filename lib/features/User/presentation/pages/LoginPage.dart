import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:web_application/core/constants/AppColors.dart';
import 'package:web_application/core/constants/AppTheme.dart';
import 'package:web_application/core/constants/app_assets.dart';
import 'package:sizer/sizer.dart';
import 'package:web_application/features/Home/presentation/pages/HomePage.dart';
import 'package:web_application/features/User/domain/cubit/user_cubit.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:web_application/features/User/presentation/pages/SignUpPage.dart';
import 'package:web_application/features/User/presentation/widgets/PasswordTextField.dart';
import 'package:web_application/features/User/presentation/widgets/SignInButton.dart';
import 'package:web_application/features/User/presentation/widgets/UsernameTextField.dart';

class LoginPage extends StatelessWidget {
  static const routeName = '/LoginPage';

  TextEditingController _usernameController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();

  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => UserCubit(),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightBlue,
          title: Center(
            child: Text(
              "تسجيل دخول",
              textAlign: TextAlign.center,
            ),
          ),
        ),
        body: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/intro_0.jpg"),
                  fit: BoxFit.cover,
//                  colorFilter: ColorFilter.mode(AppTheme.appGreyColor, BlendMode.color),
                ),
              ),
              padding: EdgeInsets.symmetric(horizontal: 20.0.h),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20.0.h),
              color: AppColors.darkText.withOpacity(0.9),
            ),

            Container(
              child: Scaffold(
                // resizeToAvoidBottomPadding: false,
                // resizeToAvoidBottomInset: false,
                extendBodyBehindAppBar: true,
                backgroundColor: Colors.transparent,
//              appBar: AppBar(
//                elevation: 0,
//                backgroundColor: AppColors.blue2,
//                title: Center(
//                    child: Text(
//                      "sign_in".tr(),
//                      style: TextStyle(fontSize: 25.0,
//                        color: AppColors.lightText
//
//                      ),
//                    )),
//              ),

                body: Container(
                  child: Padding(
                    padding: EdgeInsets.all(20.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        //If Keyboard is open
                        if (MediaQuery.of(context).viewInsets.bottom == 0)
                          SizedBox(height: 15.0)
                        else
                          SizedBox(height: 1.0),

                        _buildForm(),
                        _buildConsumer(),
                        _buildSignUp(context)
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _buildConsumer() {
    //  return _buildButton();
    return BlocConsumer<UserCubit, UserState>(
        // cubit: userCubit,
        listener: (context, state) {
      if (state is LoginError)
        Scaffold.of(context)
            .showSnackBar(SnackBar(content: Text(state.message)));
      else if (state is LoginSuccessfully) print("object");
    }, builder: (context, state) {
      if (state is LoginLoading)
        return CircularProgressIndicator();
      else
        return _buildButton(context);
    });
  }

  _buildForm() {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(0, 1.0, 0, 1.0),
            child: UsernameTextField(
              action: TextInputAction.next,
              textEditingController: _usernameController,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 1.0, bottom: 5),
            child: PasswordTextField(
              action: TextInputAction.done,
              textEditingController: _passwordController,
            ),
          )
        ],
      ),
    );
  }

  _buildButton(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 3.0),
      child: SignInButton(
        onPressed: () => _onPressLoginButton(context),
        title: "sign_in".tr(),
      ),
    );
  }

  _onPressLoginButton(BuildContext context) {
    if (_formKey.currentState.validate()) {
//      context
//          .read<UserCubit>()
//          .login(_usernameController.text, _passwordController.text);
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => HomePage()));
    }
  }

  _buildSignUp(BuildContext context) {
    return TextButton(
      child: Text(
        'إنشاء حساب'.tr(),
        style: AppTheme.caption,
      ),
      onPressed: () => Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => SignUpPage(),
        ),
      ),
    );
  }
}
