
import 'package:web_application/core/CoreModels/base_result_model.dart';

class LoginResponseModel extends BaseResultModel{
  String accessToken;
  String encryptedAccessToken;
  int expireInSeconds;
  int userId;
  String language;
  String userName;
  String roleName;

  LoginResponseModel(
      {this.accessToken,
        this.encryptedAccessToken,
        this.expireInSeconds,
        this.userId,
        this.language,
        this.userName,
        this.roleName});

  LoginResponseModel.fromJson(Map<String, dynamic> json) {
    accessToken = json['accessToken'];
    encryptedAccessToken = json['encryptedAccessToken'];
    expireInSeconds = json['expireInSeconds'];
    userId = json['userId'];
    language = json['language'];
    userName = json['userName'];
    roleName = json['roleName'];
  }

}