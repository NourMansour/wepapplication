import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';

import 'package:flutter/services.dart';
import 'package:sizer/sizer.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:web_application/core/constants/AppColors.dart';
import 'package:web_application/core/utils/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:web_application/features/Home/presentation/pages/HomePage.dart';
import 'package:web_application/features/User/presentation/pages/LoginPage.dart';



class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    super.initState();
//    checkInternet();
    startTimer();
  }

  void startTimer() {
    Timer(Duration(seconds: 3), () {
      navigateUser(); //It will redirect  after 3 seconds
    });
  }

//  void checkInternet()async{
//    try {
//      final result = await InternetAddress.lookup('google.com');
//      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
//        print('connected');
//        startTimer();
//      }
//    } on SocketException catch (_) {
//      showDialog(
//        context: context,
//        builder: (context) => AlertDialog(
//          shape: RoundedRectangleBorder(
//              borderRadius: BorderRadius.all(Radius.circular(20.0))),
//          title: Text("Unable to connect to the Internet".tr()),
//          actions: <Widget>[
//            FlatButton(
//              onPressed: () => {
//                Navigator.pop(context),
//                checkInternet(),
//              },
//              child: Text("try".tr(),style: TextStyle(color: AppColors.blue2),),
//            ),
//            FlatButton(
//              onPressed: () => exit(0),
//              /*Navigator.of(context).pop(true)*/
//              child: Text("Close".tr(),style: TextStyle(color: Colors.red),),
//            ),
//          ],
//        ),
//      );
//    }
//  }
//
  void navigateUser() async {
//AppSharedPreferences.hasAccessToken
      if (true) {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => LoginPage()));
      } else {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => LoginPage()));
      }


  }

  @override
  Widget build(BuildContext context) {
//    /*     change color status bar    */
//    //Now we use SystemChrome
//    SystemChrome.setSystemUIOverlayStyle(
//      SystemUiOverlayStyle(
//        //Lets make the Status Bar Transparent
//        statusBarColor: AppColors.blue2,
//
//        //Lets make the status bar icon brightness to bright
//        statusBarIconBrightness: Brightness.light,
//      ),
//    );
//    ModalRoute.withName('/');
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 10.0.h,
              ),

              Container(
                  height: 15.0.h,
                  child: RichText(
                    text: TextSpan(
                      text: "اهلاً وسهلاً".tr(),
                      style: TextStyle(
                          color:  Colors.lightBlue, fontSize: 20),
                    ),
                  )),
              Hero(
                tag: "splash",
                child: Container(
                  width: 90.0.w,
                  height: 40.0.h,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("assets/images/logo.png"),
                      )),
                ),
              ),
              SizedBox(
                height: 8.0.h,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 25.0.w),
                child: LinearProgressIndicator(
                  backgroundColor:  Colors.lightBlue,
                  minHeight: 6,
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
