import 'base_error.dart';

class ConnectionError extends BaseError {
  String message ='ConnectionError Message';
  @override
  List<Object> get props => [];
}