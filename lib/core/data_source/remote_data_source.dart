import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:web_application/core/CoreModels/base_result_model.dart';
import 'package:web_application/core/utils/SharedPreferences/SharedPreferencesHelper.dart';
import '../constants.dart';
import '../http/api_provider.dart';
import '../http/api_urls.dart';
import '../http/http_method.dart';
// import '../utils/shared_preference_utils/shared_preferences.dart';


 class RemoteDataSource {
   
  static Future<BaseResultModel> request<Response extends BaseResultModel>({
    @required Response Function(Map<String, dynamic> json) converter,
    @required HttpMethod method,
    @required String url,
    Map<String, dynamic> queryParameters,
    Map<String, dynamic> data,
    bool withAuthentication = false,
    CancelToken cancelToken,
  }) async {

    assert(converter != null);
    assert(method != null);
    assert(url != null);

    Map<String, String> headers= {
      "Content-Type" : "application/json",
      "accept" : "application/json",
    };

    /// Get the language.
    String lang = AppSharedPreferences.lang;
    headers.putIfAbsent(HEADER_LANGUAGE, () => lang);


    if (withAuthentication) {
      if (AppSharedPreferences.hasAccessToken)
        headers.putIfAbsent(HEADER_AUTH, () => ('bearer ${AppSharedPreferences.accessToken}'));


    }
    //ToDo remove prints
    print('HEADERS  $headers');

    print('--------------------  Start Request Body  --------------------'
        ' \n The HttpMethod & URL are : [$method: ${ApiURLs.BASE_URL}$url]');
    print('The sended Object Request is : $data');
    print('The headers is : $headers');
    print('The queryParameters is : $queryParameters \n '
        '--------------------  End Request Body  --------------------'
        ' \n');

    // Send the request.
    final response = await ApiProvider.sendObjectRequest<Response>(
      method: method,
      url:   url,
      converter: converter,
      headers: headers,
      queryParameters: queryParameters ?? {},
      data: data,
      cancelToken: cancelToken,
    );

    if (response.success) {
      return response.result;
    }
    else
    {
      if(response.serverError != null)
      return response.serverError;
      else
      return response.error;
    }
  }

//ToDO fix this
//   Future<BaseResultModel> uploadFile<Response extends BaseResultModel>({
//     @required String responseStr,
//     @required Response Function(Map<String, dynamic>) converter,
//     @required String url,
//     @required String fileKey,
//     @required String filePath,
//     ProgressCallback onSendProgress,
//     ProgressCallback onReceiveProgress,
//     bool withAuthentication = false,
//     CancelToken cancelToken,
//   }) async {
//     assert(responseStr != null);
//     assert(converter != null);
//     assert(url != null);
//
//     // Register the response.
//     ModelsFactory.getInstance().registerModel(
//       responseStr,
//       converter,
//     );
//
//     // Specify the headers.
//     final Map<String, String> headers = {
//       'Content-Type': 'multipart/form-data',
//       "Accept": "*/*",
//       "Encoding": "gzip, deflate, br",
//       "Connection": "keep-alive"
//     };
//
//     // Get the language.
//     String lang = appSharedPrefs.lang;
//     headers.putIfAbsent(HEADER_LANGUAGE, () => lang);
// //    headers.putIfAbsent(HEADER_AUTH, () => ('bearer ${appSharedPrefs.token}'));
//
//     print('headers:::: $headers');
//
//     // Send the request.
//     var response;
//     try {
//       response = await ApiProvider.upload<Response>(
//         url: url,
//         fileKey: fileKey,
//         filePath: filePath,
//         fileName: filePath?.substring(filePath.lastIndexOf('/') + 1),
//         headers: headers,
//         onSendProgress: onSendProgress,
//         onReceiveProgress: onReceiveProgress,
//         cancelToken: cancelToken,
//       );
//     } catch (e) {
//       print(e);
//       return response;
//     }
//
//     print('responseeee $response');
//
//     if (response.isLeft()) {
//       return Left((response as Left<BaseError, Response>).value);
//     } else if (response.isRight()) {
//       final resValue = (response as Right<BaseError, Response>).value;
//       return Right(resValue);
//       // if (resValue.isOk != null && resValue.isOk) {
//       //   return Right(resValue);
//       // } else if (resValue.isOk != null && !resValue.isOk) {
//       //   return Left(CustomError(message: resValue.message.content));
//       // }
//      // return null;
//     }
//     return null;
//   }


 }
