import 'dart:convert';
import 'dart:io';

import 'package:web_application/core/CoreModels/base_response_model.dart';
import 'package:web_application/core/CoreModels/base_result_model.dart';

import 'api_urls.dart';
import 'package:flutter/foundation.dart';
import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';
import 'http_method.dart';
import '../errors/base_error.dart';
import '../errors/bad_request_error.dart';
import '../errors/cancel_error.dart';
import '../errors/conflict_error.dart';
import '../errors/forbidden_error.dart';
import '../errors/http_error.dart';
import '../errors/internal_server_error.dart';
import '../errors/net_error.dart';
import '../errors/not_found_error.dart';
import '../errors/socket_error.dart';
import '../errors/timeout_error.dart';
import '../errors/unauthorized_error.dart';
import '../errors/unknown_error.dart';

class ApiProvider {

  // static Future<BaseResponseModel> upload<T extends BaseResultModel>({
  //   @required T Function(Map<String, dynamic>) converter,
  //   @required String url,
  //   @required String fileKey,
  //   @required String filePath,
  //   @required String fileName,
  //   Map<String, dynamic> data,
  //   Map<String, String> headers,
  //   ProgressCallback onSendProgress,
  //   ProgressCallback onReceiveProgress,
  //   String modelName,
  //   CancelToken cancelToken,
  // }) async{
  //   assert(url != null);
  //   assert(fileKey != null);
  //   var option =  BaseOptions(
  //     baseUrl: ApiURLs.BASE_URL,
  //     connectTimeout: 4000,
  //     sendTimeout: 20000,
  //     receiveTimeout: 4000
  //   );
  //   final _dio = Dio(option);
  //   Map<String, dynamic> dataMap = {};
  //   if(data != null){
  //     dataMap.addAll(data);
  //   }
  //   if(filePath != null && fileName != null){
  //     dataMap.addAll({
  //       fileKey:
  //       await MultipartFile.fromFile(
  //         filePath,
  //         filename: fileName,
  //         contentType: MediaType("image","jpeg")
  //       )});
  //   }
  //   try {
  //     final response = await _dio.post(
  //       url,
  //       data: FormData.fromMap(dataMap),
  //       onSendProgress: onSendProgress,
  //       onReceiveProgress: onReceiveProgress,
  //       options: Options(headers: headers,

  //       ),
  //       cancelToken: cancelToken,
  //     );

  //     var decodedJson;
  //     if (response.data is String)
  //       decodedJson = json.decode(response.data);
  //     else
  //       decodedJson = response.data;

  //     print('decodedJson $decodedJson');

  //     return  BaseResponseModel.fromJson(json: decodedJson,fromJson: converter);

  //   }
  //   on DioError catch (e) {
  //     return BaseResponseModel.fromJson(json: e.request.data,error: _handleDioError(e));
  //   }

  //   // Couldn't reach out the server
  //   on SocketException catch (e, stacktrace) {
  //     print(e);
  //     print(stacktrace);
  //     return BaseResponseModel.fromJson(error: SocketError());

  //   }
  //   catch(e , stacktrace) {
  //     print(e);
  //     print(stacktrace);
  //     return BaseResponseModel.fromJson(error: SocketError());
  //   }

  // }

  static Future<BaseResponseModel> sendObjectRequest<T extends BaseResultModel>({
    @required T Function(Map<String, dynamic>) converter,
    @required HttpMethod method,
    @required String url,
    Map<String, dynamic> data,
    Map<String, String> headers,
    Map<String, dynamic> queryParameters,
 //   bool withAuthentication = false,
    CancelToken cancelToken,
  }) async {
    assert(method != null);
    assert(url != null);

    var baseOptions =  BaseOptions(
        connectTimeout: 5000,
    );

    String u =ApiURLs.BASE_URL+ ApiURLs.O_AUTH_LOGIN;
    print('full url');
    print(u);

    Options options =Options(headers: headers,
    method: method.toString().split('.').last,
      contentType: Headers.jsonContentType,
      sendTimeout: 4000,
    );

    try {
      // Get the response from the server
      Response response;
      response = await Dio(baseOptions).request(
          u,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
        data: data
      );
      // Get the decoded json
      var decodedJson;

      if (response.data is String )
       {
         if(response.data.length ==0)
           decodedJson={"":""};
         else
           decodedJson = json.decode(response.data);
       }
      else
        decodedJson = response.data;

     // print('decodedJson');
     // print(decodedJson);

      // Return the http response with actual data
      return BaseResponseModel.fromJson(json: decodedJson,fromJson: converter);
    }

    // Handling errors
    on DioError catch (e , s) {
      print('catch DioError ');
      print(e);

      print('Stacktrace DioError ');
      print(s);
     var error = _handleDioError(e);
      var json ;
      print('DioErrorDioErrorDioError $error');
      if(e.response!=null )
        if(e.response.data!=null)
          if(!(e.response.data is String))
           {
             print(e.response.data);
             json =e.response.data;
           }

      return BaseResponseModel.fromJson(json: json,error: error);
    }

    // Couldn't reach out the server
    on SocketException catch (e, stacktrace) {
      print(e);
      print(stacktrace);
      return BaseResponseModel.fromJson(error: SocketError());
    }
    catch(e , stacktrace) {
      print(e);
      print(stacktrace);
      return BaseResponseModel.fromJson(error: SocketError());
    }
  }


  static BaseError _handleDioError(DioError error) {
    print('error.type = ${(error.type) }');
    if (error.type == DioErrorType.DEFAULT ||
        error.type == DioErrorType.RESPONSE) {
      if (error is SocketException) return SocketError();
      if (error.type == DioErrorType.RESPONSE) {
        switch (error.response.statusCode) {
          case 400:
            return BadRequestError();
          case 401:
            return UnauthorizedError();
          case 403:
            return ForbiddenError();
          case 404:
            return NotFoundError();
          case 409:
            return ConflictError();
          case 500:
            return InternalServerError();

          default:
            return HttpError();
        }
      }
      return NetError();
    } else if (error.type == DioErrorType.CONNECT_TIMEOUT ||
        error.type == DioErrorType.SEND_TIMEOUT ||
        error.type == DioErrorType.RECEIVE_TIMEOUT) {
      return TimeoutError();
    } else if (error.type == DioErrorType.CANCEL) {
      return CancelError();
    } else
      return UnknownError();
  }
}
