import 'package:flutter/material.dart';
import 'package:web_application/core/constants/AppColors.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:sizer/sizer.dart';

class BaseTextField extends StatelessWidget {
  final TextEditingController textEditingController;
  final TextInputAction action;
  final InputDecoration decoration;
  final String labelText;
  final Widget suffixIcon;
  final obscureText ;
  const BaseTextField({
    Key key,
    this.textEditingController,
    this.action,
    this.decoration,
    this.labelText,
    this.suffixIcon,
    this.obscureText = false
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final node = FocusScope.of(context);

    return TextFormField(
      obscureText: obscureText,
      controller: textEditingController,
      textInputAction: action,
      onEditingComplete: () {
        if ( action == TextInputAction.next)
          node.nextFocus();
        else
          node.unfocus();
      },

      cursorColor: AppColors.cyan21,

      decoration: decoration==null?inputDecoration():decoration
      ,
      validator: (value) => validator(value),
    );



  }

  validator(value) {
    if (value.length < 5) {
      return 'Please_enter_a_longer_x'.tr(args: [labelText]);
    } else if (value.length > 32) {
      return 'x_must_not_be_longer_than_y_characters'.tr(args: [labelText, '32']);
    }
    return null;
  }

  inputDecoration()
  {
    return InputDecoration(
        enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white)),
        labelText: labelText,
        labelStyle: TextStyle(
            fontSize: 14.0.sp,
                color: Colors.white
        ),

        suffixIcon: suffixIcon

      // border: OutlineInputBorder(
      //     borderRadius: BorderRadius.circular(7.0)),
    );
  }

}



