import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

buildSnackbar(String msg) {
  return Builder(
    builder: (BuildContext context) {
      'text'.tr();
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Test snackbar done! $msg'),
      ));
      return Container();
    },
  );
}
