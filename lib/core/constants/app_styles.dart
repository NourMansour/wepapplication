// import 'dimens.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:google_fonts/google_fonts.dart';
// import 'app_colors.dart';

// class AppStyles {
//   AppStyles._();

//   static get appBarTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 20 * Dimens.zoom),
//         fontWeight: FontWeight.bold,
//         color: Colors.white,
//       );

//   static get appWhiteTitleTextStyle => GoogleFonts.cairo(
//     textStyle: TextStyle(fontSize: 20 * Dimens.zoom),
//     fontWeight: FontWeight.bold,
//     color: Colors.white,
//   );

//   static get sideMenuTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 16 * Dimens.zoom),
//         color: Colors.white,
//       );

//   static get appInfoTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 16 * Dimens.zoom),
//         color: Colors.grey,
//       );

//   static get developedByTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 16 * Dimens.zoom),
//         fontWeight: FontWeight.bold,
//         color: AppColors.secondaryColor,
//       );

//   static get linkTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 16 * Dimens.zoom),
//         color: Colors.blueAccent,
//         decoration: TextDecoration.underline,
//       );

//   static get dialKeysTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 12 * Dimens.zoom),
//       );

//   static get tableHeaderTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 14 * Dimens.zoom),
//         fontWeight: FontWeight.bold,
//       );

//   static get tableBodyTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 12 * Dimens.zoom),
//       );

//   static get categoryListItemTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 20 * Dimens.zoom),
//         color: AppColors.primaryColor,
//         fontWeight: FontWeight.bold,
//       );

//   static get telephoneCardTitleTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 18 * Dimens.zoom),
//         color: AppColors.secondaryColor,
//         fontWeight: FontWeight.bold,
//       );

//   static get telephoneCardSubtitleTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 14 * Dimens.zoom),
//         color: Colors.grey.shade400,
//       );

//   static get subcategoryGridCardTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 14 * Dimens.zoom),
//         color: AppColors.secondaryColor,
//         fontWeight: FontWeight.bold,
//       );

//   static get subcategoryListItemTitleTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 16 * Dimens.zoom),
//         fontWeight: FontWeight.bold,
//         color: AppColors.secondaryColor,
//       );

//   static get subcategoryListItemItemsCountTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 14 * Dimens.zoom),
//         color: Colors.grey,
//       );

//   static get dialogOkButtonTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 14 * Dimens.zoom),
//         color: Colors.white,
//       );

//   static get dialogCancelButtonTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 14 * Dimens.zoom),
//         color: AppColors.primaryColor,
//       );

//   static get dialogColoredAreaTitleTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 16 * Dimens.zoom),
//       );

//   static get chipTitleTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 14 * Dimens.zoom),
//         fontWeight: FontWeight.normal,
//         color: AppColors.primaryColor,
//       );

//   static get chipSelectedTitleTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 14 * Dimens.zoom),
//         fontWeight: FontWeight.normal,
//         color: Colors.white,
//       );

//   static get loadingTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 18 * Dimens.zoom),
//         color: AppColors.primaryColor,
//       );

//   static get errorTitleTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 14 * Dimens.zoom),
//         color: Colors.grey,
//       );

//   static get placeCardTitleTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 16 * Dimens.zoom),
//         color: AppColors.primaryColor,
//       );

//   static get placeCardLocationTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 14 * Dimens.zoom),
//         color: Colors.grey,
//       );

//   static get placeCardCallNowTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 12 * Dimens.zoom),
//         color: AppColors.primaryColor,
//       );

//   static get placeCardViewsTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 12 * Dimens.zoom),
//         color: Colors.grey,
//       );

//   static get filterDialogTitleTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 18 * Dimens.zoom),
//       );

//   static get filterDialogSubtitleTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 14 * Dimens.zoom),
//       );

//   static get filterDialogHintTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 14 * Dimens.zoom),
//         color: Colors.grey,
//       );

//   static get filterDialogDropdownItemTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 14 * Dimens.zoom),
//       );

//   static get searchBarHintStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 16 * Dimens.zoom),
//         color: Colors.grey,
//       );

//   static get searchBarTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 16 * Dimens.zoom),
//       );

//   static get placeDetailsNameTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 18 * Dimens.zoom),
//         color: AppColors.primaryColor,
//       );

//   static get placeDetailsDescTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 14 * Dimens.zoom),
//       );

//   static get placeDetailsLocationTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 14 * Dimens.zoom),
//       );

//   static get placeDetailsShowMapTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 14 * Dimens.zoom),
//         fontWeight: FontWeight.normal,
//         color: Colors.grey,
//       );

//   static get placeDetailsViewsTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 12 * Dimens.zoom),
//         color: Colors.grey,
//       );

//   static get placeDetailsCategoryTextStyle => GoogleFonts.cairo(
//         textStyle: TextStyle(fontSize: 18 * Dimens.zoom),
//       );
// }
