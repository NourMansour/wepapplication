class AppAssets {
  AppAssets._();

  static const PatientAvatar = 'assets/images/Avatar.png';

  //SVG
  static const heartStethoscope = 'assets/images/heart_stethoscope.svg';
  static const medicalCollections = 'assets/images/medical_collections.svg';
}
