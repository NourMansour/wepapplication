import 'package:flutter/material.dart';

import 'AppColors.dart';


class AppTheme
{

  static const AppBarTheme appBarTheme = AppBarTheme(
      color: AppColors.cyan3
  );
  static const TextTheme textTheme = TextTheme(
    headline4: headline4,
    headline5: headline5,
    headline6: headline6,
    subtitle2: subtitle2,
    bodyText2: bodyText2,
    bodyText1: bodyText1,
    caption: caption,
  );
  static const String fontMarcellus = 'Marcellus';
  static const String fontRoboto = 'Roboto';


  static const TextStyle headline4 = TextStyle(
    fontFamily: fontMarcellus,
    fontWeight: FontWeight.bold,
    fontSize: 36,
    letterSpacing: 0.4,
    height: 0.9,
    color: AppColors.darkerText,
  );

  static const TextStyle headline5 = TextStyle(
    fontFamily: fontRoboto,
    //fontWeight: FontWeight.bold,
    fontSize: 24,
    letterSpacing: 0.27,
    color: AppColors.darkerText,
  );

  static const TextStyle headline6 = TextStyle(
    fontFamily: fontMarcellus,
    fontWeight: FontWeight.bold,
    fontSize: 16,
    letterSpacing: 0.18,
    color: AppColors.darkerText,
  );

  static const TextStyle subtitle2 = TextStyle(
    fontFamily: fontMarcellus,
    fontWeight: FontWeight.w400,
    fontSize: 14,
    letterSpacing: -0.04,
    color: AppColors.darkText,
  );

  static const TextStyle bodyText2 = TextStyle(
    fontFamily: fontMarcellus,
    fontWeight: FontWeight.w400,
    fontSize: 15,
//    letterSpacing: 0.2,
    color: AppColors.darkText,
  );

  static const TextStyle bodyText1 = TextStyle(
    fontFamily: fontMarcellus,
    fontWeight: FontWeight.w400,
    fontSize: 16,
    letterSpacing: -0.05,
    color: AppColors.darkText,
  );

  static const TextStyle caption = TextStyle(
    fontFamily: fontMarcellus,
    fontWeight: FontWeight.w400,
    fontSize: 16,
    letterSpacing: 0.2,
    color: AppColors.lightText, // was lightText
  );


}