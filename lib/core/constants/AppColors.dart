import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';


class AppColors {

  static const Color transparent = Color(0x00000000);
  static const Color cyan11= Color(0xFFE3F6FE);
  static const Color cyan12 = Color(0xFFBDF6F8);
  static const Color cyan13 = Color(0xFF74EDF0);


  static const Color cyan21 = Color(0xFF61CFBF);
  static const Color cyan22 = Color(0xFFC5E7E2);
  static const Color cyan23 = Color(0xFFE6F4F2);

  static const Color cyan3 = Color(0xFF87D1C6);

  static const Color blue1 = Color(0xFF3A549E);
  static const Color blue2 = Color(0xFF5A78C0);
  static const Color blue3 = Color(0xFF849FEF);

  static List<Color> linear = [
    Color(0xFF87D1C6),
    Color(0xFFC5E7E2),
    Color(0xFFE6F4F2)
  ];

  static const Color red = Color(0xFFF78481); //Colors.redAccent[100]
  static const Color gray = Color(0xFFA4A4A4);




  //Text Theme
  static const Color lightText = Color(0xFF979797);
  static const Color darkText = Color(0xFF4A4A4A);
  static const Color darkerText = Color(0xFF17262A);


}