import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer_util.dart';
import 'package:web_application/features/EditProperty/presentation/pages/EditProperty.dart';

import 'package:web_application/features/Home/presentation/pages/HomePage.dart';
import 'package:web_application/features/splash/presentation/pages/Splash.dart';
import 'core/constants.dart';
import 'core/constants/AppIcons.dart';
import 'core/utils/SharedPreferences/SharedPreferencesHelper.dart';
import 'features/User/presentation/pages/LoginPage.dart';
import 'core/constants/AppColors.dart';
import 'core/constants/AppTheme.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await AppSharedPreferences.init();


  String lang= AppSharedPreferences.lang;
  if(lang == null)
    {
      AppSharedPreferences.lang =DEFAULT_LANG;
      lang = DEFAULT_LANG;
    }


  runApp(
    EasyLocalization(
        startLocale: Locale(lang),
        supportedLocales: [Locale(LANG_AR) ,Locale(LANG_EN)],
        path: 'assets/lang',
        fallbackLocale: Locale(LANG_AR),
        child: MyApp()
    ),
  );
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Sizer(
      child: MaterialApp(
        title: 'Web Application',
        theme: ThemeData(

          //todo remove these random color

          highlightColor: AppColors.cyan3,
          primaryColor: AppColors.cyan21,//cyan23
          primaryColorDark:  Color(0xff201f39),

          backgroundColor: Colors.green,
          canvasColor: Colors.white,

          accentColor: Colors.lightBlue,


          appBarTheme: AppTheme.appBarTheme,
          iconTheme: IconThemeData(color: AppColors.cyan21 ),


          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
routes: {
  EditProperty.routeName: (context) => EditProperty(),
},
         home: Splash( ),

        debugShowCheckedModeBanner: false,
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        locale: context.locale,
      ),
    );
  }
}
//responsive
class Sizer extends StatelessWidget {
  final Widget child;

  const Sizer({Key key, this.child}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return OrientationBuilder(
          builder: (context, orientation) {
            SizerUtil().init(constraints, orientation);
            return child;
          },
        );
      },
    );
  }
}

